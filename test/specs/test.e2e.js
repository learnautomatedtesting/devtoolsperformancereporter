import LoginPage from '../pageobjects/login.page.js';
import { expect, browser, $ } from '@wdio/globals';
import fs from 'fs-extra';

let xhrData = [];
let pendingRequests = {};
let allData = {};
let xhrCount = 0;
let lastXHRInitiationTime = 0;
let lighthouseMetricsArray = [];
let isLighthouseAuditRunning = false;  // Added this flag

function structurePerformanceData(label, metricsData, networkData, fullLoadTime, lighthouseMetrics) {
    return {
        [label]: {
            metrics: metricsData,
            network: networkData,
            fullLoadTime: fullLoadTime,
            lighthouseMetrics: lighthouseMetrics
        }
    };
}

async function captureAndAggregatePerformanceData(browser, strvalue) {
    const currentUrl = await browser.getUrl();

    if (!currentUrl || currentUrl === 'about:blank') {
        throw new Error("No URL loaded in the browser before attempting to capture metrics.");
    }

    const screenshotPath = `./reportgenerator/screenshotoutput/${strvalue}.png`;
    await browser.saveScreenshot(screenshotPath);

    const navigationTimings = await browser.execute(() => performance.getEntriesByType('navigation')[0]);
    const fullLoadTime = navigationTimings.loadEventEnd - navigationTimings.fetchStart;

    const performanceMetrics = await browser.cdp('Performance', 'getMetrics');
    
    await browser.enablePerformanceAudits();

    isLighthouseAuditRunning = true;  // Set the flag to true when Lighthouse audit starts
    await browser.url(currentUrl);
    const lighthouseMetrics = await browser.getMetrics();
    isLighthouseAuditRunning = false; // Reset the flag once Lighthouse audit is done
    lighthouseMetricsArray.push(lighthouseMetrics);

    await browser.disablePerformanceAudits();

    const aggregatedData = structurePerformanceData(strvalue, performanceMetrics, xhrData, fullLoadTime, lighthouseMetrics);
    xhrData = [];
    return aggregatedData;
}

describe('My Login application', () => {
    before(async () => {
        await browser.cdp('Network', 'enable');
        await browser.cdp('Performance', 'enable');

        browser.on('Network.requestWillBeSent', (params) => {
            if (params.type === 'XHR' && !isLighthouseAuditRunning) {  // Check for isLighthouseAuditRunning here
                xhrCount++;
                lastXHRInitiationTime = new Date().getTime();
                pendingRequests[params.requestId] = params.timestamp;
            }
        });

        browser.on('Network.responseReceived', (params) => {
            if (params.type === 'XHR' && !isLighthouseAuditRunning) {  // Check for isLighthouseAuditRunning here
                xhrCount--;
                const duration = params.timestamp - pendingRequests[params.requestId];
                xhrData.push({
                    url: params.response.url,
                    statusCode: params.response.status,
                    duration: duration * 1000
                });
                delete pendingRequests[params.requestId];
            }
        });
    });

    it('should login with valid credentials', async () => {
        await LoginPage.open();
        await waitForXHRs();
        
        Object.assign(allData, await captureAndAggregatePerformanceData(browser, 'BeforeLoggingIn'));
        xhrData = [];
        
        await LoginPage.login('Admin', 'admin123');
        const strwait = $('//*[contains(@href,"dashboard")]');
        await strwait.waitForDisplayed();
        
        await waitForXHRs();
        
        Object.assign(allData, await captureAndAggregatePerformanceData(browser, 'AfterLoggingIn'));
        xhrData = [];

        fs.writeFileSync('./reportgenerator/allData.json', JSON.stringify(allData, null, 2));
    });

    after(async () => {
        await browser.cdp('Network', 'disable');
    });
});

async function waitForXHRs(gracePeriod = 3000) {
    await browser.waitUntil(() => {
        const now = new Date().getTime();
        return xhrCount === 0 && (now - lastXHRInitiationTime > gracePeriod);
    }, {
        timeout: 20000,
        timeoutMsg: 'XHRs did not finish within the expected time.'
    });
}
