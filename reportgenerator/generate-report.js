import fs from 'fs-extra';
import path from 'path';

const INPUT_FILE = 'allData.json';
const OUTPUT_FILE = 'report.html';

function getSmileyBasedOnTimedom(time) {
    if (time <= 2000) return "😊";
    else if (time > 2000 && time <= 7000) return "😐";
    else return "😟";
}

function getSmileyBasedOnTimenetwork(time) {
    if (time <= 1000) return "😊";
    else if (time > 1000 && time <= 3000) return "😐";
    else return "😟";
}

function getOverallSmiley(domTime, networkTimes) {
    const domSmiley = getSmileyBasedOnTimedom(domTime);
    const networkSmileys = networkTimes.map(time => getSmileyBasedOnTimenetwork(time));
    
    if (domSmiley === "😟" || networkSmileys.includes("😟")) {
        return "😟";
    } else if (domSmiley === "😐" || networkSmileys.includes("😐")) {
        return "😐";
    } else {
        return "😊";
    }
}

function getColorForMetric(metric, value) {
    console.log(`Checking metric: ${metric} with value: ${value}`); 
    switch (metric) {
        case 'domContentLoaded':
        case 'firstContentfulPaint':
            return value <= 1000 ? "🟢" : (value <= 3000 ? "🟠" : "🔴");
        case 'largestContentfulPaint':
        case 'Interactive':
            return value <= 2500 ? "🟢" : (value <= 4000 ? "🟠" : "🔴");
        case 'cumulativeLayoutShift':
            return value <= 0.1 ? "🟢" : (value <= 0.25 ? "🟠" : "🔴");
        case 'network':
            return value <= 1000 ? "🟢" : (value <= 3000 ? "🟠" : "🔴");
        default:
            return "🟢";  // Default to green if the metric isn't recognized
    }
}

function generateHTML(data) {
    let htmlContent = `
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Performance Metrics Dashboard</title>
        <style>
        body {
            background-image: linear-gradient(180deg, rgba(58,131,215,0.80) 0%, rgba(24,169,209,0.80) 100%), url('pictures/background.png');
            background-size: cover;
            background-repeat: no-repeat;
            background-attachment: fixed;
            color: white; 
            font-weight: bold; 
            position: relative; 
            padding-top: 60px; /* Added this line */
        }

        .content-container {
            overflow-y: auto; /* This ensures content inside this div is scrollable */
            height: calc(100% - 60px); /* Adjusting for the padding-top we added to body */
            position: relative;
        }
        
        a {
            color: white;
            text-decoration: none;
        }
        
        a:hover {
            text-decoration: underline;
        }
        
        #metricsContainer h3 {
            margin-top: 20px;
        }
        
        #metricsContainer p {
            margin: 5px 0;
        }
        
        table {
            width: 100%;
            border-collapse: collapse;
            margin: 20px 0;
        }
        
        th, td {
            border: 1px solid white; 
            text-align: left;
            padding: 8px;
        }
        
        #legend {
            position: fixed;
            top: 10px;
            right: 10px;
        }
        
        #legend p {
            font-size: 18px;
        }
        
        #logo {
            position: fixed;
            top: 10px;
            left: 10px;
            height: 50px; 
            width: auto;
            z-index: 10;
            background-color: transparent; 
        }
        
        </style>
    </head>
    <body>
        <img id="logo" src="pictures/logo.png" alt="Company Logo">

        <div class="content-container">

        <div id="legend" style="font-size: 12px;">
        <h3>Threshold Legend</h3>
        <table border="1" cellspacing="0" cellpadding="5">
            <tr>
                <td>First Contentful Paint (FCP)</td>
                <td>🟢 <= 1000ms</td>
                <td>🟡 = 1000-3000ms</td>
                <td>🔴 > 3000ms</td>
            </tr>
            <tr>
                <td>Interactive Contentful Paint (LCP)</td>
                <td>🟢 <= 2500ms</td>
                <td>🟡 = 2500-4000ms</td>
                <td>🔴 > 4000ms</td>
            </tr>
            <tr>
                <td>Interactive</td>
                <td>🟢 <= 3800ms</td>
                <td>🟡 = 3800-7300ms</td>
                <td>🔴 > 7300ms</td>
            </tr>
            <tr>
                <td>Cumulative Layout Shift (CLS)</td>
                <td>🟢 <= 0.1</td>
                <td>🟡 = 0.1-0.25</td>
                <td>🔴 > 0.25</td>
            </tr>
            <tr>
                <td>Max Potential FID (First Input Delay)</td>
                <td>🟢 <= 100ms</td>
                <td>🟡 = 100-300ms</td>
                <td>🔴 > 300ms</td>
            </tr>
            <tr>
                <td>Network Time</td>
                <td>🟢 <= 1000ms</td>
                <td>🟡 = 1001-3000ms</td>
                <td>🔴 > 3000ms</td>
            </tr>
        </table>
        <p>For explanation regarding the legend, check <a href="https://learnautomatedtesting.com/blog/googlelighthouse/" target="_blank">my website</a>.</p>
    </div>
    
        <h2 style="padding-top: 70px;">Performance Metrics</h2>
        <div id="metricsContainer">`;



    for (let label of Object.keys(data)) {
        const item = data[label];
        const networkDurations = item.network.map(net => net.duration);
        const overallSmiley = getOverallSmiley(item.domContentLoadedTime, networkDurations);

        // Generate screenshot file name based on label
        const screenshotFilename = `./screenshotoutput/${label}.png`;

        htmlContent += `
            <h3><a href="#" onclick="const content = document.getElementById('${label}'); content.style.display = content.style.display === 'none' ? 'block' : 'none'; return false;">${label} ${overallSmiley}</a></h3>
            <div id="${label}" style="display: none;">
                <img src="${screenshotFilename}" alt="Screenshot for ${label}" width="300" style="margin: 10px 0;">
                 <p>DomContentLoadedTime: ${item.lighthouseMetrics.domContentLoaded} ms ${getColorForMetric('domContentLoaded', item.lighthouseMetrics.domContentLoaded)}</p>
                 <p>First Contentful Paint: ${item.lighthouseMetrics.firstContentfulPaint} ms ${getColorForMetric('firstContentfulPaint', item.lighthouseMetrics.firstContentfulPaint)}</p>
                 <p>Largest Contentful Paint: ${item.lighthouseMetrics.largestContentfulPaint} ms ${getColorForMetric('largestContentfulPaint', item.lighthouseMetrics.largestContentfulPaint)}</p>
                 <p>Interactive: ${item.lighthouseMetrics.interactive} ms ${getColorForMetric('Interactive', item.lighthouseMetrics.interactive)}</p>
                 <p>Cumulative Layout Shift: ${item.lighthouseMetrics.cumulativeLayoutShift} ${getColorForMetric('cumulativeLayoutShift', item.lighthouseMetrics.cumulativeLayoutShift)}</p> 
                <table>
                    <thead>
                        <tr>
                            <th>URL</th>
                            <th>Status Code</th>
                            <th>Duration (ms)</th>
                            <th>Smiley</th>
                        </tr>
                    </thead>
                    <tbody>`;

        for (let networkItem of item.network) {
            htmlContent += `
                <tr>
                    <td>${networkItem.url}</td>
                    <td>${networkItem.statusCode}</td>
                    <td>${networkItem.duration}</td>
                    <td>${getSmileyBasedOnTimenetwork(networkItem.duration)}</td>
                </tr>`;
        }

        htmlContent += `
                    </tbody>
                </table>
            </div>`;
    }

    htmlContent += `
        </div>
        </div>
    </body>
    </html>`;

    return htmlContent;
}

const jsonData = fs.readFileSync(path.resolve(INPUT_FILE), 'utf-8');
const parsedData = JSON.parse(jsonData);
const htmlContent = generateHTML(parsedData);

fs.writeFileSync(path.resolve(OUTPUT_FILE), htmlContent);
