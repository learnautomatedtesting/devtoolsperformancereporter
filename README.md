# Devtoolsperformancereporter


## Project status
This can be used for demo purposes, and is open source. Feel free to use this for your own needs. I am busy with a paid custom solution in AWS lambda/rds/grid, so feel free to ask for this and the pricing plan





## Getting started
start with npm i 

then run the test npx wdio\\

then run node generate-report.js

## explanation regarding the functional (first draft)


Of course! Let's break down this code in plain English:

Introduction:
This code is about testing the performance of a web application while interacting with it. It uses WebDriverIO to interact with the browser and test the "login" process. Throughout the testing, it captures and records how long certain actions (like XHRs - which are basically web requests) take, and also fetches some other metrics related to the performance.

Detailing the script

Imports: At the very top, some modules and functionalities are imported. This includes the login page module, some WebDriverIO global functions, and the filesystem module for saving data.

Global Variables: A few variables are set up to keep track of network requests and performance data. These will be used later in the code.

structurePerformanceData Function: This function helps organize the performance data in a readable format.

captureAndAggregatePerformanceData Function: This is a crucial function. It captures various performance metrics from the currently loaded web page and structures them. Steps include:

Capturing a screenshot.
Fetching navigation timings like when the content loaded and total time for the web page to load.
Fetching other metrics related to performance.
Structuring this data to be saved.
describe 'My Login application': This section sets up and runs the main test:

before: Before the main test runs:
Network and Performance are enabled for capturing data.
Two listeners are set up to detect when an XHR request starts and when it finishes, recording relevant data.
it 'should login with valid credentials': This is the main test. In this test:
The login page is opened.
Performance data is captured and saved under the label 'BeforeLoggingIn'.
The code simulates a login action with credentials 'Admin' and 'admin123'.
Once logged in, it waits until a dashboard element appears.
Performance data is again captured and saved under the label 'AfterLoggingIn'.

Finally, all the captured data is saved into a file named 'allData.json'.
after: Once everything is done, the network data capturing is turned off.
waitForXHRs Function: This is a helper function. When called, it waits until all ongoing XHR requests are completed and then waits for an additional grace period.
qwe
the user can run a report node generate-report.js

Bear in mind, this is just a very simple demo to get inspiration. If you need more details then please request this via form on my website https:/learnautomatedtesting.com via my contact me!

